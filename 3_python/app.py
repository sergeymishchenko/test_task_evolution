#!/usr/bin/env python 
import time
import os
import logging
import sys
from kafka import KafkaProducer, KafkaConsumer
from datetime import datetime, timezone
import rfc3339
import prometheus_client

_logger = logging.getLogger(__name__)

def metric_expose(role, metric_name):
  _logger.debug("Getting metrics from kafka-python...")
  _logger.debug(f"role used is {role}")
  _logger.debug(f"getting metric {metric_name}")
  if role == "producer":
    metric = producer.metrics()['producer-metrics'][metric_name]
  elif role == "consumer":
    metric = consumer.metrics()['consumer-metrics'][metric_name]
  KAFKA_USAGE.labels(f'{metric_name}').set(metric)



def kafka_producer(kafka_host, kafka_topic):
  _logger.info("App is started as a plain kafka producer...")
  global producer
  producer = KafkaProducer(bootstrap_servers=kafka_host)
  while True:
    msg = bytes(str(round(time.time() * 1000)), 'utf-8')
    producer.send(kafka_topic, msg)
    metric_expose('producer', 'byte-rate')
    metric_expose('producer', 'io-wait-ratio')


def kafka_consumer_producer(kafka_host, kafka_topic_consume, kafka_topic_produce):
  _logger.info("App is stared with a mixed consumer/producer profile...")
  global consumer
  consumer = KafkaConsumer(bootstrap_servers=kafka_host)
  consumer.subscribe([kafka_topic_consume])
  global producer
  producer = KafkaProducer(bootstrap_servers=kafka_host)
  while True:
    for message in consumer:
      message = message.value
      decoded = (round(int(message.decode('utf-8')) / 1000))
      decoded_format = rfc3339.rfc3339(decoded)
      msg = bytes(str(decoded_format), 'utf-8')
      producer.send(kafka_topic_produce, msg)
      metric_expose('producer', 'byte-rate')
      metric_expose('consumer', 'request-latency-avg')

        
if __name__ == '__main__':
  
  if os.environ.get('LOG_LEVEL') is None:
    logging.basicConfig(level=logging.INFO)
  else:
    logging.basicConfig(level=logging.os.environ.get('LOG_LEVEL'))

  prometheus_client.start_http_server(8000)

  profile = os.environ.get('PROFILE')

  if profile == 'producer':
    KAFKA_USAGE = prometheus_client.Gauge('kafka_producer', 'app kafka metrics', ['metric'])
    kafka_producer(os.environ.get('KAFKA_HOST'), os.environ.get('KAFKA_TOPIC_WRITE'))
  elif profile == 'mixed':
    KAFKA_USAGE = prometheus_client.Gauge('kafka_mixed', 'app kafka metrics', ['metric'])
    kafka_consumer_producer(os.environ.get('KAFKA_HOST'), os.environ.get('KAFKA_TOPIC_READ'), os.environ.get('KAFKA_TOPIC_WRITE'))
  else:
    _logger.error("PROFILE variable has not been set, exiting...")
    sys.exit(1)
